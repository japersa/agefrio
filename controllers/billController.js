var billModel = require('../models/billModel.js');
var customerModel = require('../models/customerModel.js');

/**
 * billController.js
 *
 * @description :: Server-side logic for managing bills.
 */
module.exports = {

  /**
   * billController.list()
   */
  list: function (req, res) {
    billModel.find(function (err, bills) {
      customerModel.populate(bills, {
        path: "customer"
      }, function (err, bills) {
        if (err) {
          return res.json(500, {
            message: 'Error getting bills.'
          });
        }
        return res.json(bills);
      });
    });
  },

  /**
   * billController.show()
   */
  show: function (req, res) {
    var code = req.params.code;
    billModel.findOne({
      code: code
    }, function (err, bill) {
      customerModel.populate(bill, {
        path: "customer"
      }, function (err, bill) {
        if (err) {
          return res.json(500, {
            message: 'Error getting bill.'
          });
        }
        if (!bill) {
          return res.json(404, {
            message: 'No such bill'
          });
        }
        return res.status(200).json(bill);
      });

    });
  },

  /**
   * billController.generateBillCode()
   */
  generateBillCode: function (req, res) {
    // add zeros to left
    function zeroPad(num, places) {
      var zero = places - num.toString().length + 1;
      return Array(+(zero > 0 && zero)).join("0") + num;
    }

    billModel.count({}, function (err, count) {
      if (err) {
        return res.json(500, {
          message: 'Error getting bills.'
        });
      }

      res.json({
        billCode: "A" + zeroPad(count + 4, 4),
        currentDate: new Date(),
        iva: 16
      });

    });
  },

  /**
   * billController.create()
   */
  create: function (req, res) {
    req.checkBody('customer', 'customer is required').notEmpty();
    req.checkBody('bill_code', 'bill_code is required').notEmpty();
    req.checkBody('products', 'products is required').notEmpty();
    req.checkBody('subtotal', 'subtotal is required').notEmpty();
    req.checkBody('total_iva', 'total_iva is required').notEmpty();
    req.checkBody('total', 'total is required').notEmpty();

    var errors = req.validationErrors();
    if (!errors) {
      billModel.findOne({
        bill_code: req.body.bill_code
      }, function (err, bill) {
        if (err) {
          return res.status(500).json({
            message: 'Error getting bill',
            error: err
          });
        }
        if (bill) {
          return res.status(500).json({
            message: 'Bill code already exist'
          });
        }

        var bill = new billModel({
          code: new Date().getTime(),
          customer: req.body.customer,
          bill_customer: req.body.bill_customer,
          bill_code: req.body.bill_code,
          products: req.body.products,
          subtotal: req.body.subtotal,
          total_iva: req.body.total_iva,
          total: req.body.total
        });

        bill.save(function (err, bill) {
          if (err) {
            return res.status(500).json({
              message: 'Error saving bill',
              error: err
            });
          }
          return res.json({
            message: 'saved',
            code: bill.code,
            _id: bill._id
          });
        });
      });
    } else {
      return res.status(500).json({
        title: 'Validation create customer',
        message: 'Failure to create customer due to some validation error',
        errors: errors
      });
    }
  }
};