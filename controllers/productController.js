var productModel = require('../models/productModel.js');
var settingModel = require('../models/settingModel.js');
/**
 * productController.js
 *
 * @description :: Server-side logic for managing products.
 */
module.exports = {

    /**
     * productController.list()
     */
    list: function (req, res) {
        productModel.find(function (err, products) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting product.'
                });
            }
            return res.json(products);
        });
    },

    /**
     * productController.listStatus()
     */
    listStatus: function (req, res) {
        var status = req.params.status;

        productModel.find({
            status: status
        }, function (err, products) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting product.'
                });
            }
            return res.json(products);
        });
    },

    /**
     * productController.show()
     */
    show: function (req, res) {
        var code = req.params.code;

        productModel.findOne({
            code: code
        }, function (err, product) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting product.'
                });
            }
            if (!product) {
                return res.status(404).json({
                    message: 'No such product'
                });
            }
            return res.json(product);
        });
    },
    /*
     * productController.generateProductCode()
     */
    generateProductCode: function (req, res) {
        settingModel.findOne({
            type: 'product'
        }, function (err, setting) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting setting.'
                });
            }
            if (!setting) {
                return res.status(404).json({
                    message: 'No such setting'
                });
            }
            var start = setting.settings.start;
            var limit = setting.settings.limit;
            productModel.count({}, function (err, count) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting bills.'
                    });
                }
                var ref = start * 1 + count * 1;
                if (ref <= limit) {
                    res.json({
                        ref: ref
                    });
                } else {
                    res.status(500).json({
                        message: 'You have exceeded the limit of product'
                    });
                }
            });
        });
    },
    /**
     * productController.create()
     */
    create: function (req, res) {

        req.checkBody('code', 'code is required').notEmpty();
        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('price', 'price is required').notEmpty();
        req.checkBody('ref', 'ref is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            productModel.findOne({
                name: req.body.name.toLowerCase()
            }, function (err, product) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting product',
                        error: err
                    });
                }
                if (product) {
                    return res.status(500).json({
                        message: 'Product already exist'
                    });
                }
                var product = new productModel({
                    ref: req.body.ref,
                    code: req.body.code,
                    name: req.body.name.toLowerCase(),
                    description: req.body.description ? req.body.description.toLowerCase() : "",
                    price: req.body.price
                });

                product.save(function (err, product) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error saving product',
                            error: err
                        });
                    }
                    return res.json({
                        message: 'saved',
                        _id: product._id
                    });
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation create product',
                message: 'Failure to create product due to some validation error',
                errors: errors
            });
        }
    },

    /**
     * productController.update()
     */
    update: function (req, res) {
        var code = req.params.code;

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('price', 'price is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {

            productModel.findOne({
                code: code
            }, function (err, product) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting product',
                        error: err
                    });
                }
                if (!product) {
                    return res.status(404).json({
                        message: 'No such product'
                    });
                }

                var result = product;

                productModel.findOne({
                    name: req.body.name.toLowerCase()
                }, function (err, product) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error getting product',
                            error: err
                        });
                    }

                    if (product) {
                        if (product.code == code) {
                            result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                            result.description = req.body.description ? req.body.description.toLowerCase() : "";
                            result.price = req.body.price ? req.body.price : result.price;
                            result.updated_at = new Date();

                            result.save(function (err, product) {
                                if (err) {
                                    return res.json(500, {
                                        message: 'Error saving product.'
                                    });
                                }
                                if (!product) {
                                    return res.status(404).json({
                                        message: 'No such product'
                                    });
                                }
                                return res.json(product);
                            });
                        } else {
                            return res.status(500).json({
                                message: 'Product already exist'
                            });
                        }
                    } else {
                        result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                        result.description = req.body.description ? req.body.description.toLowerCase() : "";
                        result.price = req.body.price ? req.body.price : result.price;
                        result.updated_at = new Date();

                        result.save(function (err, product) {
                            if (err) {
                                return res.json(500, {
                                    message: 'Error saving product.'
                                });
                            }
                            if (!product) {
                                return res.status(404).json({
                                    message: 'No such product'
                                });
                            }
                            return res.json(product);
                        });
                    }
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation update product',
                message: 'Failure to update product due to some validation error',
                errors: errors
            });
        }
    },

    /**
     * productController.status()
     */
    status: function (req, res) {
        var code = req.params.code;

        req.checkBody('status', 'status is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            productModel.findOneAndUpdate({
                code: code
            }, {
                status: req.body.status
            }, function (err, product) {
                if (err) {
                    return res.json(500, {
                        message: 'Error getting product.'
                    });
                }

                if (!product) {
                    return res.status(404).json({
                        message: 'No such product'
                    });
                }

                return res.json(product);
            });
        } else {
            return res.status(500).json({
                title: 'Validation status product',
                message: 'Failure to status product due to some validation error',
                errors: errors
            });
        }
    }
};