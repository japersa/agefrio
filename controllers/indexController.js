var billModel = require('../models/billModel.js');
var customerModel = require('../models/customerModel.js');
var productModel = require('../models/productModel.js');
var serviceModel = require('../models/serviceModel.js');


/**
 * billController.js
 *
 * @description :: Server-side logic for managing bills.
 */
module.exports = {

  /**
   * billController.show()
   */
  show: function(req, res) {
    var data = {};
    billModel.count(function(err, bills) {
      if (err) {
        return res.json(500, {
          message: 'Error getting bills.'
        });
      }
      data.bills = bills;
      customerModel.count(function(err, customers) {
        if (err) {
          return res.json(500, {
            message: 'Error getting customers.'
          });
        }
        data.customers = customers;
        productModel.count(function(err, products) {
          if (err) {
            return res.json(500, {
              message: 'Error getting services.'
            });
          }
          data.products = products;
          serviceModel.count(function(err, services) {
            if (err) {
              return res.json(500, {
                message: 'Error getting services.'
              });
            }
            data.services = services;
            res.json(data);
          });
        });
      });
    });
  }

};
