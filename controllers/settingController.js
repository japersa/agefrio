var settingModel = require('../models/settingModel.js');

/**
 * settingController.js
 *
 * @description :: Server-side logic for managing settings.
 */
module.exports = {

  /**
   * settingController.list()
   */
  list: function(req, res) {
    settingModel.find(function(err, settings) {
      if (err) {
        return res.json(500, {
          message: 'Error getting setting.'
        });
      }
      return res.json(settings);
    });
  },

  /**
   * settingController.show()
   */
  show: function(req, res) {
    var type = req.params.type;
    settingModel.findOne({
      type: type
    }, function(err, setting) {
      if (err) {
        return res.json(500, {
          message: 'Error getting setting.'
        });
      }

      return res.json(setting);
    });
  },

  /**
   * settingController.create()
   */
  create: function(req, res) {
    req.checkBody('code', 'code is required').notEmpty();
    req.checkBody('type', 'type is required').notEmpty();
    req.checkBody('settings', 'settings is required').notEmpty();

    var errors = req.validationErrors();
    if (!errors) {
      settingModel.findOne({
        type: req.body.type
      }, function(err, setting) {
        if (err) {
          return res.json(500, {
            message: 'Error getting setting.'
          });
        }
        if (setting) {
          return res.json(500, {
            message: 'Setting already exist'
          });
        }
        var setting = new settingModel({
          code: req.body.code,
          type: req.body.type,
          settings: req.body.settings
        });

        setting.save(function(err, setting) {
          if (err) {
            return res.json(500, {
              message: 'Error saving setting',
              error: err
            });
          }
          return res.json({
            message: 'saved',
            _id: setting._id
          });
        });
      });
    } else {
      return res.status(500).json({
        title: 'Validation create setting',
        message: 'Failure to create setting due to some validation error',
        errors: errors
      });
    }
  },

  /**
   * settingController.update()
   */
  update: function(req, res) {
    var type = req.params.type;
    settingModel.findOne({
      type: type
    }, function(err, setting) {
      if (err) {
        return res.json(500, {
          message: 'Error saving setting',
          error: err
        });
      }
      if (!setting) {
        return res.json(404, {
          message: 'No such setting'
        });
      }

      setting.settings = req.body.settings ? req.body.settings : setting.settings;

      setting.save(function(err, setting) {
        if (err) {
          return res.json(500, {
            message: 'Error getting setting.'
          });
        }
        if (!setting) {
          return res.json(404, {
            message: 'No such setting'
          });
        }
        return res.json(setting);
      });
    });
  }
};
