var identificationTypeModel = require('../models/identificationTypeModel.js');

/**
 * identificationTypeController.js
 *
 * @description :: Server-side logic for managing identificationTypes.
 */
module.exports = {

    /**
     * identificationTypeController.list()
     */
    list: function (req, res) {
        identificationTypeModel.find(function (err, identificationTypes) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting identificationType.'
                });
            }
            return res.json(identificationTypes);
        });
    },

    /**
     * identificationTypeController.listStatus()
     */
    listStatus: function (req, res) {
        var status = req.params.status;

        identificationTypeModel.find({
            status: status
        }, function (err, identificationTypes) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting identificationType.'
                });
            }
            return res.json(identificationTypes);
        });
    },

    /**
     * identificationTypeController.show()
     */
    show: function (req, res) {
        var code = req.params.code;
        identificationTypeModel.findOne({
            code: code
        }, function (err, identificationType) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting identificationType.'
                });
            }
            if (!identificationType) {
                return res.status(404).json({
                    message: 'No such identificationType'
                });
            }
            return res.json(identificationType);
        });
    },

    /**
     * identificationTypeController.create()
     */
    create: function (req, res) {
        req.checkBody('code', 'code is required').notEmpty();
        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('abbreviation', 'abbreviation is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {

            identificationTypeModel.findOne({
                name: req.body.name.toLowerCase()
            }, function (err, identificationType) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting identificationType',
                        error: err
                    });
                }
                if (identificationType) {
                    return res.status(500).json({
                        message: 'identificationType already exist'
                    });
                }

                var identificationType = new identificationTypeModel({
                    code: req.body.code,
                    name: req.body.name.toLowerCase(),
                    abbreviation: req.body.abbreviation.toUpperCase()
                        // description: req.body.description.toLowerCase()
                });

                identificationType.save(function (err, identificationType) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error saving identificationType',
                            error: err
                        });
                    }
                    return res.json({
                        message: 'saved',
                        _id: identificationType._id
                    });
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation create identificationType',
                message: 'Failure to create identificationType due to some validation error',
                errors: errors
            });
        }
    },

    /**
     * identificationTypeController.update()
     */
    update: function (req, res) {
        var code = req.params.code;

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('abbreviation', 'abbreviation is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            identificationTypeModel.findOne({
                code: code
            }, function (err, identificationType) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting identificationType',
                        error: err
                    });
                }
                if (!identificationType) {
                    return res.json(404, {
                        message: 'No such identificationType'
                    });
                }

                var result = identificationType;

                identificationTypeModel.findOne({
                    name: req.body.name.toLowerCase()
                }, function (err, identificationType) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error getting identificationType',
                            error: err
                        });
                    }
                    if (identificationType) {
                        if (identificationType.code == code) {
                            result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                            result.abbreviation = req.body.abbreviation.toUpperCase() ? req.body.abbreviation : result.abbreviation;
                            result.description = req.body.description.toLowerCase() ? req.body.description : result.description;
                            result.updated_at = new Date();

                            result.save(function (err, identificationType) {
                                if (err) {
                                    return res.status(500).json({
                                        message: 'Error saving identificationType.'
                                    });
                                }
                                if (!identificationType) {
                                    return res.status(404).json({
                                        message: 'No such identificationType'
                                    });
                                }
                                return res.json(identificationType);
                            });
                        } else {
                            return res.status(500).json({
                                message: 'identificationType already exist'
                            });
                        }
                    } else {
                        result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                        result.abbreviation = req.body.abbreviation.toUpperCase() ? req.body.abbreviation : result.abbreviation;
                        result.description = req.body.description.toLowerCase() ? req.body.description : result.description;
                        result.updated_at = new Date();

                        result.save(function (err, identificationType) {
                            if (err) {
                                return res.status(500).json({
                                    message: 'Error saving identificationType.'
                                });
                            }
                            if (!identificationType) {
                                return res.status(404).json({
                                    message: 'No such identificationType'
                                });
                            }
                            return res.json(identificationType);
                        });
                    }
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation update identificationType',
                message: 'Failure to update identificationType due to some validation error',
                errors: errors
            });
        }
    },

    /**
     * identificationTypeController.status()
     */
    status: function (req, res) {
        var code = req.params.code;

        req.checkBody('status', 'status is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            identificationTypeModel.findOneAndUpdate({
                code: code
            }, {
                status: req.body.status
            }, function (err, identificationType) {
                if (err) {
                    return res.json(500, {
                        message: 'Error getting identificationType.'
                    });
                }

                if (!identificationType) {
                    return res.status(404).json({
                        message: 'No such identificationType'
                    });
                }

                return res.json(identificationType);
            });
        } else {
            return res.status(500).json({
                title: 'Validation status identificationType',
                message: 'Failure to status identificationType due to some validation error',
                errors: errors
            });
        }
    }
};