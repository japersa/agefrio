var serviceModel = require('../models/serviceModel.js');
var settingModel = require('../models/settingModel.js');
/**
 * serviceController.js
 *
 * @description :: Server-side logic for managing services.
 */
module.exports = {

    /**
     * serviceController.list()
     */
    list: function (req, res) {
        serviceModel.find(function (err, services) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting services.'
                });
            }
            return res.json(services);
        });
    },

    /**
     * serviceController.listStatus()
     */
    listStatus: function (req, res) {
        var status = req.params.status;

        serviceModel.find({
            status: status
        }, function (err, services) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting services.'
                });
            }
            return res.json(services);
        });
    },
    /*
     * serviceController.generateProductCode()
     */
    generateServiceCode: function (req, res) {
        settingModel.findOne({
            type: 'service'
        }, function (err, setting) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting setting.'
                });
            }
            if (!setting) {
                return res.status(404).json({
                    message: 'No such setting'
                });
            }
            var start = setting.settings.start;
            var limit = setting.settings.limit;
            serviceModel.count({}, function (err, count) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting bills.'
                    });
                }
                var ref = start * 1 + count * 1;
                if (ref <= limit) {
                    res.json({
                        ref: ref
                    });
                } else {
                    res.status(500).json({
                        message: 'You have exceeded the limit of service'
                    });
                }
            });
        });
    },
    /**
     * serviceController.show()
     */
    show: function (req, res) {
        var code = req.params.code;

        serviceModel.findOne({
            code: code
        }, function (err, service) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting service.'
                });
            }
            if (!service) {
                return res.status(404).json({
                    message: 'No such service'
                });
            }
            return res.json(service);
        });
    },

    /**
     * serviceController.create()
     */
    create: function (req, res) {

        req.checkBody('code', 'code is required').notEmpty();
        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('price', 'price is required').notEmpty();
        req.checkBody('ref', 'ref is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            serviceModel.findOne({
                name: req.body.name.toLowerCase()
            }, function (err, service) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting service',
                        error: err
                    });
                }
                if (service) {
                    return res.status(500).json({
                        message: 'Service already exist'
                    });
                }
                var service = new serviceModel({
                    ref: req.body.ref,
                    code: req.body.code,
                    name: req.body.name.toLowerCase(),
                    description: req.body.description ? req.body.description.toLowerCase() : "",
                    price: req.body.price
                });

                service.save(function (err, service) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error saving service',
                            error: err
                        });
                    }
                    return res.json({
                        message: 'saved',
                        _id: service._id
                    });
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation create service',
                message: 'Failure to create service due to some validation error',
                errors: errors
            });
        }
    },

    /**
     * serviceController.update()
     */
    update: function (req, res) {
        var code = req.params.code;
        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('price', 'price is required').notEmpty();
        var errors = req.validationErrors();
        if (!errors) {

            serviceModel.findOne({
                code: code
            }, function (err, service) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting service',
                        error: err
                    });
                }
                if (!service) {
                    return res.status(404).json({
                        message: 'No such service'
                    });
                }

                var result = service;

                serviceModel.findOne({
                    name: req.body.name.toLowerCase()
                }, function (err, service) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error getting service',
                            error: err
                        });
                    }

                    if (service) {
                        if (service.code == code) {
                            result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                            result.description = req.body.description ? req.body.description.toLowerCase() : "";
                            result.price = req.body.price ? req.body.price : result.price;
                            result.updated_at = new Date();

                            result.save(function (err, service) {
                                if (err) {
                                    return res.json(500, {
                                        message: 'Error saving service.'
                                    });
                                }
                                if (!service) {
                                    return res.status(404).json({
                                        message: 'No such service'
                                    });
                                }
                                return res.json(service);
                            });
                        } else {
                            return res.status(500).json({
                                message: 'Service already exist'
                            });
                        }
                    } else {
                        result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                        result.description = req.body.description ? req.body.description.toLowerCase() : "";
                        result.price = req.body.price ? req.body.price : result.price;
                        result.updated_at = new Date();

                        result.save(function (err, service) {
                            if (err) {
                                return res.json(500, {
                                    message: 'Error saving service.'
                                });
                            }
                            if (!service) {
                                return res.status(404).json({
                                    message: 'No such service'
                                });
                            }
                            return res.json(service);
                        });
                    }
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation update service',
                message: 'Failure to update service due to some validation error',
                errors: errors
            });
        }
    },

    /**
     * serviceController.status()
     */
    status: function (req, res) {
        var code = req.params.code;

        req.checkBody('status', 'status is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            serviceModel.findOneAndUpdate({
                code: code
            }, {
                status: req.body.status
            }, function (err, service) {
                if (err) {
                    return res.json(500, {
                        message: 'Error getting service.'
                    });
                }

                if (!service) {
                    return res.status(404).json({
                        message: 'No such service'
                    });
                }

                return res.json(service);
            });
        } else {
            return res.status(500).json({
                title: 'Validation status service',
                message: 'Failure to status service due to some validation error',
                errors: errors
            });
        }
    }
};