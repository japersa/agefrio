var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var productSchema = new Schema({	"code" : { type: String, unique: true, require: true },	"ref" : { type: String, unique: true, require: true },	"name" : { type: String, require: true },	"description" : { type: String },	"price" : { type: Number, require: true },	"status": { type: Number, default: 1 },	"created_at": { type: Date, default: Date.now },	"updated_at": { type: Date }});

module.exports = mongoose.model('product', productSchema);
