var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var settingSchema = new Schema({
	"code": { type: String, unique: true, require: true },
	"type" : { type: String, unique: true, require: true },
	"settings": {}
});

module.exports = mongoose.model('setting', settingSchema);
