var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var identificationTypeModel = require('../models/identificationTypeModel.js');

var customerSchema = new Schema({	"code" : { type: String, unique: true, require: true },  "identification_type": { type: Schema.ObjectId, ref: 'identificationTypeModel' },  "id_document" : { type: String, unique: true, require: true },	"name" : { type: String, require: true },  "address" : { type: String },	"city" : { type: String },  "email": { type: String, require: true },	"phone" : { type: String, require: true },	"mobile_phone" : { type: String },  "status": { type: Number, default: 1 },  "created_at": { type: Date, default: Date.now },  "updated_at": { type: Date }});

module.exports = mongoose.model('customer', customerSchema);
