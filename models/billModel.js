var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var customerModel = require('../models/customerModel.js');

var billSchema = new Schema({
	"code": { type: String, unique: true, require: true },
	"current_date": { type: Date },
	"customer": { type: Schema.ObjectId, ref: 'customerModel' },
    "bill_customer": {},
    "bill_code": { type: String, require: true, unique: true },
	"products": [],
	"subtotal": { type: Number, require: true },
	"total_iva": { type: Number, require: true },
	"total": { type: Number, require: true },
	"copy_number": { type: Number },
	"status": { type: Number, default: 1 },
	"created_at": { type: Date, default: Date.now },
	"updated_at": { type: Date }
});


module.exports = mongoose.model('bill', billSchema);
