var express = require('express');
var router = express.Router();
var productController = require('../controllers/productController.js');

/*
 * GET ALL PRODUCTS
 */
router.get('/', function (req, res) {
    productController.list(req, res);
});

/*
 * GET
 */
router.get('/status/:status', function (req, res) {
    productController.listStatus(req, res);
});

/*
 * GET
 */
router.get('/product/code', function(req, res) {
    productController.generateProductCode(req, res);
});


/*
 * GET SINGLE PRODUCT
 */
router.get('/:code', function (req, res) {
    productController.show(req, res);
});

/*
 * CREATE PRODUCT
 */
router.post('/', function (req, res) {
    productController.create(req, res);
});

/*
 * UPDATE PRODUCT
 */
router.put('/:code', function (req, res) {
    productController.update(req, res);
});

/*
 * CHANGE PRODUCT STATUS
 */
router.put('/status/:code', function (req, res) {
    productController.status(req, res);
});

module.exports = router;
