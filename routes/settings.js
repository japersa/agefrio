var express = require('express');
var router = express.Router();
var settingController = require('../controllers/settingController.js');

/*
 * GET
 */
router.get('/', function(req, res) {
    settingController.list(req, res);
});

/*
 * GET
 */
router.get('/:type', function(req, res) {
    settingController.show(req, res);
});

/*
 * POST
 */
router.post('/', function(req, res) {
    settingController.create(req, res);
});

/*
 * PUT
 */
router.put('/:type', function(req, res) {
    settingController.update(req, res);
});


module.exports = router;
