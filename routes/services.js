var express = require('express');
var router = express.Router();
var serviceController = require('../controllers/serviceController.js');

/*
 * GET ALL SERVICES
 */
router.get('/', function (req, res) {
    serviceController.list(req, res);
});

/*
 * GET
 */
router.get('/status/:status', function (req, res) {
    serviceController.listStatus(req, res);
});

/*
 * GET SINGLE SERVICE
 */
router.get('/:code', function (req, res) {
    serviceController.show(req, res);
});

/*
 * GET
 */
router.get('/service/code', function(req, res) {
    serviceController.generateServiceCode(req, res);
});

/*
 * CREATE SERVICE
 */
router.post('/', function (req, res) {
    serviceController.create(req, res);
});

/*
 * UPDATE SERVICE
 */
router.put('/:code', function (req, res) {
    serviceController.update(req, res);
});

/*
 * CHANGE STATUS
 */
router.put('/status/:code', function (req, res) {
    serviceController.status(req, res);
});

module.exports = router;
