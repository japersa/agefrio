var express = require('express');
var router = express.Router();
var customerController = require('../controllers/customerController.js');

/*
 * GET all customers
 */
router.get('/', function (req, res) {
    customerController.list(req, res);
});

/*
 * GET
 */
router.get('/status/:status', function (req, res) {
    customerController.listStatus(req, res);
});

/*
 * GET single customer
 */
router.get('/:code', function (req, res) {
    customerController.show(req, res);
});

/*
 * CREATE new customer
 */
router.post('/', function (req, res) {
    customerController.create(req, res);
});

/*
 * UPDATE customer
 */
router.put('/:code', function (req, res) {
    customerController.update(req, res);
});

/*
 * CHANGE customer status
 */
router.put('/status/:code', function (req, res) {
    customerController.status(req, res);
});


module.exports = router;