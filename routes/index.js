var express = require('express');
var router = express.Router();
var indexController = require('../controllers/indexController.js');

/*
* GET
*/
router.get('/', function(req, res) {
  indexController.show(req, res);
});

module.exports = router;
