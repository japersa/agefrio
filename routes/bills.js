var express = require('express');
var router = express.Router();
var billController = require('../controllers/billController.js');

/*
 * GET
 */
router.get('/', function(req, res) {
    billController.list(req, res);
});

/*
 * GET
 */
router.get('/:code', function(req, res) {
    billController.show(req, res);
});

/*
 * GET
 */
router.get('/bill/code', function(req, res) {
    billController.generateBillCode(req, res);
});


/*
 * POST
 */
router.post('/', function(req, res) {
    billController.create(req, res);
});


module.exports = router;
