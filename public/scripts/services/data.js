angular.module('data', [])

.factory('$Data', ['$q', '$http', function ($q, $http) {

    // Url path for production server
    var server = '/';
    var customers;
    var products;
    var services;
    var bills;

    return {
        /**
         * @description: functions to get data related with customers
         **/
        run: function () {
            var q = $q.defer();
            /* Get all the customers from BD */
            $http.get(server + 'customers/status/1').then(function (result) {
                customers = result.data;
                q.resolve('Succesfull');
            }, function (err) {
                q.reject();
            });
            return q.promise;
        },
        getAllCustomers: function () {
            return customers;
        },
        getCustomer: function (code) {
            for (var i = 0; i < customers.length; i++) {
                if (customers[i].code === code) {
                    return customers[i];
                }
            }
            return null;
        },
        /**
         * @description: functions to get data related with products
         **/
        runProducts: function () {
            var q = $q.defer();
            /* Get all the products from BD */
            $http.get(server + 'products/status/1').then(function (result) {
                products = result.data;
                q.resolve('Succesfull');
            }, function (err) {
                q.reject();
            });
            return q.promise;
        },
        getAllProducts: function () {
            return products;
        },
        getProduct: function (code) {
            for (var i = 0; i < products.length; i++) {
                if (products[i].code === code) {
                    return products[i];
                }
            }
            return null;
        },
        /**
         * @description: functions to get data related with services
         **/
        runServices: function () {
            var q = $q.defer();
            /* Get all the services from BD */
            $http.get(server + 'services/status/1').then(function (result) {
                services = result.data;
                q.resolve('Succesfull');
            }, function (err) {
                q.reject();
            });
            return q.promise;
        },
        getAllServices: function () {
            return services;
        },
        getService: function (code) {
            for (var i = 0; i < services.length; i++) {
                if (services[i].code === code) {
                    return services[i];
                }
            }
            return null;
        },
        /**
         * @description: functions to get data related with bills
         **/
        runBills: function () {
            var q = $q.defer();
            /* Get all the Bills from BD */
            $http.get(server + 'bills/').then(function (result) {
                bills = result.data;
                q.resolve('Succesfull');
            }, function (err) {
                q.reject();
            });
            return q.promise;
        },
        getAllBills: function () {
            return bills;
        },
        getBill: function (code) {
            for (var i = 0; i < bills.length; i++) {
                if (bills[i].code === code) {
                    return bills[i];
                }
            }
            return null;
        }

    }

}]);