angular.module('services', [])

    .factory('$api', ['$q', '$http', function ($q, $http) {

        // Url path for production server
        var server = '/';

        return {

            codeGen: function () {
                /*GENERATE UNIQUE CODE*/
                var d = new Date();
                var datecode = d.getTime();
                return datecode;
            },
            //*************************************************************************
            /**
             * @description: API SERVICES FOR PRODUCTS
             *
             **/

            getProducts: function () {
                /* Get all the products from BD */
                var defered = $q.defer();
                $http.get(server + 'products/').then(defered.resolve, defered.reject);
                return defered.promise;
            },
            addNew: function (object) {
                /* Create new product in BD */
                var defered = $q.defer();
                $http.post(server + 'products/', object).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            updateProduct: function (code, object) {
                /* Update product in BD */
                var defered = $q.defer();
                $http.put(server + 'products/' + code, object).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            changeProductStatus: function (code, status) {
                /* Delete product from BD */
                var defered = $q.defer();
                $http.put(server + 'products/status/' + code, status).then(defered.resolve, defered.reject);
                return defered.promise;
            },

            //*************************************************************************
            /**
             * @description: API SERVICES FOR CUSTOMERS
             *
             **/
            getIdentificationTypes: function () {
                /* Get all the identification types from BD */
                var defered = $q.defer();
                $http.get(server + 'identificationTypes/').then(defered.resolve, defered.reject);
                return defered.promise;
            },
            getCustomers: function () {
                /* Get all the products from BD */
                var defered = $q.defer();
                $http.get(server + 'customers/').then(defered.resolve, defered.reject);
                return defered.promise;
            },
            addNewCustomer: function (object) {
                /* Create new product in BD */
                var defered = $q.defer();
                $http.post(server + 'customers/', object).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            updateCustomer: function (code, object) {
                /* Update product in BD */
                var defered = $q.defer();
                $http.put(server + 'customers/' + code, object).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            changeCustomerStatus: function (code, status) {
                /* change status for customer */
                var defered = $q.defer();
                $http.put(server + 'customers/status/' + code, status).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            //*************************************************************************
            /**
             * @description: API SERVICES FOR SERVICES
             *
             **/
            getServices: function () {
                /* Get all the services from BD */
                var defered = $q.defer();
                $http.get(server + 'services/').then(defered.resolve, defered.reject);
                return defered.promise;
            },
            addNewService: function (object) {
                /* Create new services in BD */
                var defered = $q.defer();
                $http.post(server + 'services/', object).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            updateService: function (code, object) {
                /* Update services in BD */
                var defered = $q.defer();
                $http.put(server + 'services/' + code, object).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            changeServiceStatus: function (code, status) {
                /* change status for services */
                var defered = $q.defer();
                $http.put(server + 'services/status/' + code, status).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            addNewBill: function (object) {
                /* Create new bill in BD */
                var defered = $q.defer();
                $http.post(server + 'bills/', object).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            getBill: function (code) {
                /* Get bill in BD */
                var defered = $q.defer();
                $http.get(server + 'bills/' + code).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            generateBillCode: function (code) {
                /* Get bill in BD */
                var defered = $q.defer();
                $http.get(server + 'bills/bill/code').then(defered.resolve, defered.reject);
                return defered.promise;
            },
            getIndex: function (code) {
                /* Get bill in BD */
                var defered = $q.defer();
                $http.get(server + 'index/').then(defered.resolve, defered.reject);
                return defered.promise;
            },
            getAllBills: function () {
                /* Get all bills */
                var defered = $q.defer();
                $http.get(server + 'bills/').then(defered.resolve, defered.reject);
                return defered.promise;
            },
            //*************************************************************************
            /**
             * @description: API SERVICES FOR SETTINGS
             *
             **/
            addSetting: function (object) {
                /* add setting */
                var defered = $q.defer();
                $http.post(server + 'settings/', object).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            updateSetting: function (setting, object) {
                /* Update settings */
                var defered = $q.defer();
                $http.put(server + 'settings/' + setting, object).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            getSetting: function (setting) {
                /* Get settings  */
                var defered = $q.defer();
                $http.get(server + 'settings/' + setting).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            getProductCode: function () {
                /* Get product code  */
                var defered = $q.defer();
                $http.get(server + 'products/product/code').then(defered.resolve, defered.reject);
                return defered.promise;
            },
            getServiceCode: function () {
                /* Get service code  */
                var defered = $q.defer();
                $http.get(server + 'services/service/code').then(defered.resolve, defered.reject);
                return defered.promise;
            },
            createIdentification: function (object) {
                /* Create identification type  */
                var defered = $q.defer();
                $http.post(server + 'identificationTypes/', object).then(defered.resolve, defered.reject);
                return defered.promise;
            },
            getBillByCode: function (code) {
                /* Get bill data by code  */
                var defered = $q.defer();
                $http.get(server + 'bills/' + code).then(defered.resolve, defered.reject);
                return defered.promise;
            }
        }
    }]);