angular.module('settings', [])

.controller('settingsCtrl', function ($scope, $api, $timeout) {

    //Init variables
    $scope.alertTemplate = "";
    $scope.showSuccessNotification = false;
    $scope.showDangerNotification = false;
    $scope.billsettings = {};
    $scope.servicesettings = {};
    $scope.productsettings = {};

    /**
     * @description: function to load bill settings
     **/

    function getBillSettings() {

        $api.getSetting('bill').then(function (result) {

            if (!isEmpty(result.data)) {
                //no vacio
                $scope.settings = result.data;
                $scope.billsettings = result.data.settings;
            } else {
                //vacio
                $scope.settings = "";
            }

        }, function (err) {
            templateDanger("Ocurrio un error al cargar la configuración de la factura");
        });

    }

    /**
     * @description: function to load service settings
     **/


    function getServiceSettings() {
        $api.getSetting('service').then(function (result) {

            if (!isEmpty(result.data)) {
                //no vacio
                $scope.servicestgs = result.data;
                $scope.servicesettings = result.data.settings;
            } else {
                //vacio
                $scope.servicestgs = "";
            }

        }, function (err) {
            templateDanger("Ocurrio un error al cargar la configuración de los servicios");
        });
    }

    /**
     * @description: function to load product settings
     **/

    function getProductSettings() {
        $api.getSetting('product').then(function (result) {

            if (!isEmpty(result.data)) {
                //no vacio
                $scope.productstgs = result.data;
                $scope.productsettings = result.data.settings;
            } else {
                //vacio
                $scope.productstgs = "";
            }

        }, function (err) {
            templateDanger("Ocurrio un error al cargar la configuración de los productos");
        });
    }

    function loadData() {
        getBillSettings();
        getServiceSettings();
        getProductSettings();
    }

    loadData();

    function isEmpty(obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }
        return true;
    }


    $scope.save = function () {
        if ($.trim($('#iva').val()) == '') {
            $('#iva').focus();
            alert("Ingrese iva");
        } else if ($('#email').val() == '') {
            $('#email').focus();
            alert("Ingrese el email");
        } else if ($('#phone').val() == '') {
            $('#phone').focus();
            alert("Ingrese el teléfono");
        } else if ($('#resolution').val() == '') {
            $('#resolution').focus();
            alert("Ingrese el numero de Resolución DIAN");
        } else if ($('#resolution_from').val() == '') {
            $('#resolution_from').focus();
            alert("Ingrese el número inicial de facturación aprobado");
        } else if ($('#resolution_to').val() == '') {
            $('#resolution_to').focus();
            alert("Ingrese el número final de facturación aprobado");
        } else {
            $api.addSetting({
                code: new Date().getTime(),
                type: 'bill',
                settings: $scope.billsettings
            }).then(function (result) {
                templateAction("Se guardarón las configuraciones");
            }, function (err) {
                templateDanger("Ocurrio un error al guardar la configuracion");
            });
        }
    }

    $scope.edit = function (config) {
        if ($.trim($('#iva').val()) == '') {
            alert("Ingrese iva");
            $('#quantity').focus();
        } else if ($('#email').val() == '') {
            alert("Ingrese el email");
            $('#email').focus();
        } else if ($('#phone').val() == '') {
            alert("Ingrese el teléfono");
            $('#email').focus();
        } else if ($('#resolution').val() == '') {
            $('#resolution').focus();
            alert("Ingrese el numero de Resolución DIAN");
        } else if ($('#resolution_from').val() == '') {
            $('#resolution_from').focus();
            alert("Ingrese el número inicial de facturación aprobado");
        } else if ($('#resolution_to').val() == '') {
            $('#resolution_to').focus();
            alert("Ingrese el número final de facturación aprobado");
        } else {
            $api.updateSetting('bill', {
                settings: $scope.billsettings
            }).then(function (result) {
                templateAction("Se guardarón las configuraciones");
            }, function (err) {
                templateDanger("Ocurrio un error al guardar la configuración");
            });
        }
    }

    $scope.saveProductSettings = function () {
        if ($.trim($('#startproducts').val()) == '') {
            alert("Digite código inicio productos");
            $('#startproducts').focus();
        } else if ($.trim($('#limitproducts').val()) == '') {
            alert("Digite Límite código productos");
            $('#limitproducts').focus();
        } else {
            $api.addSetting({
                code: new Date().getTime(),
                type: 'product',
                settings: $scope.productsettings
            }).then(function (result) {
                templateAction("Se guardarón las configuraciones");
            }, function (err) {
                alert(JSON.stringify(err));
                templateDanger("Ocurrio un error al guardar la configuracion");
            });
        }
    }


    $scope.saveServiceSettings = function () {
        if ($.trim($('#startservices').val()) == '') {
            alert("Digite código inicio servicios");
            $('#startservices').focus();
        } else if ($.trim($('#limitservices').val()) == '') {
            alert("Digite Límite código servicios");
            $('#limitservices').focus();
        } else {
            $api.addSetting({
                code: new Date().getTime(),
                type: 'service',
                settings: $scope.servicesettings
            }).then(function (result) {
                templateAction("Se guardarón las configuraciones");
            }, function (err) {
                alert(JSON.stringify(err));
                templateDanger("Ocurrio un error al guardar la configuracion");
            });
        }
    }


    $scope.saveIdentification = function (identification) {

        if ($.trim($('#id_name').val()) == '') {
            $('#id_name').focus();
            alert("Ingrese el nombre del tipo de identificación");
        } else if ($.trim($('#id_name_abreviation').val()) == '') {
            $('#id_name_abreviation').focus();
            alert("Ingrese la abreviación del tipo de identificación");
        } else {
            $api.createIdentification({
                code: new Date().getTime(),
                name: identification.id_name,
                abbreviation: identification.abreviation
            }).then(function (result) {
                templateAction("Se guardó el tipo de identificación");
                $scope.identification = "";
            }, function (err) {
                alert(JSON.stringify(err));
                templateDanger("Ocurrio un error al guardar el tipo de identificación");
            });
        }

    }

    function templateAction(message) {
        $scope.showSuccessNotification = true;
        $scope.alertTemplate = message;
        $timeout(dismissAlert, 3000);
        loadData();
    }

    function templateDanger(message) {
        $scope.showDangerNotification = true;
        $scope.alertTemplate = message;
        $timeout(dismissAlert, 3000);
    }

    function dismissAlert() {
        $scope.showSuccessNotification = false;
        $scope.showDangerNotification = false;
        $scope.alertTemplate = "";
    }

});