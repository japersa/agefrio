angular.module('products', [])

.controller('productCtrl', function ($scope, $api, $timeout) {

    //Init variables
    $scope.alertTemplate = "";
    $scope.showSuccessNotification = false;
    $scope.showDangerNotification = false;
    //Load all products
    loadMainData();

    //*************************************************************************
    /**
     * @description: load all products
     * 
     **/

    function loadMainData() {

        $api.getProducts().then(function (result) {
            $scope.products = result.data;
        }, function (err) {
            alert("Ha ocurrido un error al cargar los productos");
        });

    }

    //*************************************************************************
    /**
     * @description: open modal for add new product, addnew function
     * 
     **/

    $scope.addNewModal = function () {

        $scope.codeProduct = $api.codeGen();

        $api.getProductCode().then(function (result) {
            $scope.ref_code = result.data.ref;
        }, function (err) {
            console.log("Ha ocurrido un error al generar la referencia del producto");
        });
    };

    $scope.addNew = function (product) {

        if ($('#reference').val() == '') {
            alert('Ocurrio un error al crear la referencia del producto');
        } else if ($('#product_value').val() == '') {
            alert('Ingrese el valor del producto');
        } else if ($('#product_value').val() == '') {
            alert('Ingrese el valor del producto');
        } else {

            var res = confirm("Deseas guardar este producto?");

            if (res) {
                $api.addNew({
                    ref: $scope.ref_code,
                    code: $scope.codeProduct,
                    name: product.name,
                    description: product.description,
                    price: product.price
                }).then(function (result) {
                    $('#productsModal').modal('hide');
                    $scope.product = {};
                    $scope.ref_code = "";
                    $scope.showSuccessNotification = true;
                    $scope.alertTemplate = "El producto se ha guardado con exito";
                    $timeout(dismissAlert, 3000);
                    loadMainData();
                }, function (err) {
                    $('#productsModal').modal('hide');
                    $scope.product = {};
                    $scope.showDangerNotification = true;
                    $scope.alertTemplate = "Ocurrio un error al guardar el producto";
                    $timeout(dismissAlert, 3000);
                });
            }
        }

    }

    //*************************************************************************
    /**
     * @description: open modal for edit product, update product function
     * 
     **/
    $scope.editModal = function (product) {
        $scope.editProduct = angular.copy(product);
        $('#editProductsModal').modal('show');
    };

    $scope.edit = function (code) {

        if ($('#name_edit').val() == '') {
            alert('Ingrese el nombre del producto');
        } else if ($('#product_value_edit').val() == '') {
            alert('Ingrese el valor del producto');
        } else {

            var res = confirm("¿Deseas actualizar este producto?");

            if (res) {
                $api.updateProduct(code, {
                    name: $scope.editProduct.name,
                    description: $scope.editProduct.description,
                    price: $scope.editProduct.price
                }).then(function (result) {
                    $('#editProductsModal').modal('hide');
                    $scope.resetFormEdit();
                    templateAction("El producto se actualizo con exito");
                }, function (err) {
                    $('#editProductsModal').modal('hide');
                    $scope.resetFormEdit();
                    $scope.showDangerNotification = true;
                    $scope.alertTemplate = "Ocurrio un error al actualizar el producto";
                    $timeout(dismissAlert, 3000);
                })

            }
        }


    };

    //*************************************************************************
    /**
     * @description: activate/desactivate product function
     * 
     **/

    $scope.desactivate = function (code) {

        var res = confirm("¿Deseas desactivar este producto?");

        if (res) {
            $api.changeProductStatus(code, {
                status: 2
            }).then(function (result) {
                templateAction("Se ha desactivado el producto");
            }, function (err) {
                templateDanger("Ocurrio un error al desactivar el producto");
            })
        }
    };

    $scope.activateProduct = function (code) {
        var res = confirm("¿Deseas activar este producto?");

        if (res) {
            $api.changeProductStatus(code, {
                status: 1
            }).then(function (result) {
                templateAction("El producto se activado con exito");
            }, function (err) {
                templateDanger("Ocurrio un error al activar el producto");
            })
        }
    };

    //*************************************************************************
    /**
     * @description: util functions
     * 
     **/

    $scope.resetFormEdit = function () {
        $scope.editProduct.name = "";
        $scope.editProduct.description = "";
        $scope.editProduct.price = "";
    };

    $scope.resetFormCreateNew = function () {
        $scope.product.name = "";
        $scope.product.description = "";
        $scope.product.price = "";
    };

    function templateAction(message) {
        $scope.showSuccessNotification = true;
        $scope.alertTemplate = message;
        $timeout(dismissAlert, 3000);
        loadMainData();
    }

    function templateDanger(message) {
        $scope.showDangerNotification = true;
        $scope.alertTemplate = message;
        $timeout(dismissAlert, 3000);
    }

    function dismissAlert() {
        $scope.showSuccessNotification = false;
        $scope.showDangerNotification = false;
        $scope.alertTemplate = "";
    }

    /**
     * @description: function to reverse array printing
     **/
    $scope.reverse = function (array) {
        var copy = [].concat(array);
        return copy.reverse();
    }

});