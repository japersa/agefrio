angular.module('getBills', [])

    .controller('getBillsCtrl', function ($scope, $state, $Data) {


        loadMainData();


        /**
         * @description: load bill data
         **/
        function loadMainData() {

            $Data.runBills().then(function (result) {
                $scope.bills = $Data.getAllBills();
            }, function (err) {
                console.log(err);
            });
        }


        /**
         * @description: open modal with detailed bill
         **/
        $scope.detailedBill = function (code) {
            /*  $scope.singleBill = $Data.getBill(code);
             $('#detailBill').modal('show'); */
            $state.go('dashboard.billDetail', {
                code: code
            })
        }


        /**
         * @description: go to print bill view
         **/
        $scope.printBill = function (code) {
            $state.go("dashboard.generateBill", {
                "code": code
            });
        }


        /**
         * @description: filter to parse day in format yyyy/mm/dd
         **/
        $scope.getDateFormat = function (timestamp) {
            var d = new Date(timestamp);
            var day = ("0" + d.getDate()).slice(-2);
            var month = ("0" + (d.getMonth() + 1)).slice(-2);
            var year = d.getFullYear();
            return day + "/" + month + "/" + year;
        }

        /**
         * @description: function to reverse array printing
         **/
        $scope.reverse = function (array) {
            var copy = [].concat(array);
            return copy.reverse();
        }

    });