angular.module('billDetail', [])

  .controller('billDetailCtrl', function ($scope, $Data, $api, $state, $stateParams, $window) {

    var index = 0;
    $scope.billCode = $stateParams.code;
    $scope.includeIva = true;
    $scope.includeOrder = false;
    $scope.selectedCustomer = "";
    $scope.billProducts = [];
    $scope.periods = [];
    $scope.selectedItem = {
      code: "",
      name: "",
      price: "",
      quantity: 0
    }
    $scope.bill = {};
    $scope.order = '';
    $scope.subtotal = 0;
    $scope.totalIva = 0;
    $scope.total = 0;

    function construct() {
      loadBillSettings();
      getServices();
      getProducts();
      getBillByCode();
      $scope.periods = [{
        'label': '15 dias',
        'val': 15
      }, {
        'label': '30 dias',
        'val': 30
      }, {
        'label': '45 dias',
        'val': 45
      }, {
        'label': '60 dias',
        'val': 60
      }];
    }

    //**************** HTTP REQUESTS ******************

    /**
     * @description: function to load bill configs
     **/
    function loadBillSettings() {
      /**
       * @description: function to load bill settings
       **/
      $api.getSetting('bill').then(function (result) {
        if (!isEmpty(result.data)) {
          $scope.iva = result.data.settings.iva;
          $scope.includeIva = true;
        } else {
          $scope.iva = "";
        }
      }, function (err) {
        console.log("Error al cargar los ajustes");
      });
    }

    /**
     * @description: function to load all products
     **/
    function getProducts() {
      $Data.runProducts().then(function (result) {
        $scope.products = $Data.getAllProducts();
      }, function (err) {
        console.log("Error al cargar los productos");
      });
    }

    /**
     * @description: function to load all services
     **/
    function getServices() {
      $Data.runServices().then(function (result) {
        $scope.services = $Data.getAllServices();
      }, function (err) {
        console.log("Error al cargar los servicios");
      });
    }

    /**
     * @description: function to load billd data according to code param
     **/
    function getBillByCode() {
      console.log("getBillByCode called");
      if (!$scope.billCode) {
        // no such as code
        console.log("Code not found");
        $state.go('dashboard.getBills');
      } else {
        $api.getBillByCode($scope.billCode).then(function (result) {
          $scope.bill = result.data;
          $scope.selectedCustomer = $scope.bill.bill_customer;
          $scope.billProducts = $scope.bill.products;
          $scope.order = $scope.bill.products[0].order;
          getValues();
          console.log("getBillByCode: ", result.data);
        }, function (err) {
          console.log("Error al cargar la factura, Code not found");
          alert('El código de factura no existe');
          $state.go('dashboard.getBills');
        });
      }
    }

    //*********************** MODALS **************************

    /**
     * @description: function to open modals
     **/

    $scope.openProductsModal = function () {
      $('#productsModal').modal('show');
    }

    $scope.openServicesModal = function () {
      $('#servicesModal').modal('show');
    }

    /**
     * @description: function to pic selected product in modal
     **/
    $scope.getSelectedProduct = function (code) {
      $Data.runProducts().then(function (result) {
        $scope.selectedItem = $Data.getProduct(code);
        $('#quantity').focus();
      }, function (err) {
        console.log("Error al cargar los productos");
      });
      $('#productsModal').modal('hide');
    }

    /**
     * @description: function to pic selected service in modal
     **/
    $scope.getSelectedService = function (code) {
      $Data.runServices().then(function (result) {
        $scope.selectedItem = $Data.getService(code);
        $('#quantity').focus();
      }, function (err) {
        console.log("Error al cargar los servicios");
      });
      $('#servicesModal').modal('hide');
    }

    //********************* RESET DATA ************************

    /**
     * @description: function to reset inputs
     **/

    $scope.resetDetail = function () {
      $scope.selectedItem = "";
    }

    /**
     * @description: funtion to clear bill
     **/

    function resetBill() {
      index = 0;
      $scope.includeIva = true;
      $scope.subtotal = 0;
      $scope.totalIva = 0;
      $scope.total = 0;
      $scope.billProducts.length = 0;
    }

    //***************** BILL CALCS AND DATA PROCESING *********************

    /**
     * @description: add product to bill details
     **/
    $scope.addItemBill = function (item) {
      var totalIvaItem = 0;
      var order = "";

      if ($scope.iva == '') {
        alert("Debes ingresar el IVA en la pestaña de configuración antes de continuar");
      } else if ($('#item_price').val() == '') {
        alert("Seleccione un producto / servicio");
      } else if ($.trim($('#quantity').val()) == '') {
        alert("Ingrese la cantidad");
        $('#quantity').focus();
      } else {
        index++;
        var amount = $scope.selectedItem.quantity * 1;
        var itemPrice = $scope.selectedItem.price * 1;

        if ($scope.includeIva) {
          totalIvaItem = ((itemPrice * amount) * ($scope.iva / 100));
        } else {
          totalIvaItem = 0;
          $scope.iva = 0;
        }

        if ($scope.includeOrder) {
          order = $scope.order;
        } else {
          order = "";
        }

        $scope.billProducts.push({
          ref: $scope.selectedItem.ref,
          code: $scope.selectedItem.code,
          name: $scope.selectedItem.name,
          price: $scope.selectedItem.price,
          quantity: $scope.selectedItem.quantity,
          order: order,
          iva: $scope.iva,
          totalIva: totalIvaItem,
          subtotal: (itemPrice * amount),
          total: ((itemPrice * amount) + totalIvaItem),
          id_index: index
        });
        //console.table($scope.billProducts);
        getValues();
        loadBillSettings();
        $scope.resetDetail();
      }
    }

    function getValues() {
      var subtotal = 0;
      var totaliva = 0;
      var total = 0;

      $scope.billProducts.forEach(function (element, index, array) {
        subtotal = (subtotal * 1 + ($scope.billProducts[index].subtotal * 1));
        totaliva = (totaliva * 1 + ($scope.billProducts[index].totalIva * 1));
        total = (total * 1 + ($scope.billProducts[index].total * 1));
      });

      $scope.subtotal = subtotal;
      $scope.totalIva = totaliva;
      $scope.total = total;
    }

    $scope.saveBill = function () {
      var response = confirm("¿Desea guardar la factura?");

      if (response) {
        $api.addNewBill({
          customer: $scope.selectedCustomer._id,
          bill_customer: $scope.selectedCustomer,
          bill_code: $scope.billCode,
          products: $scope.billProducts,
          subtotal: $scope.subtotal,
          total_iva: $scope.totalIva,
          total: $scope.total
        }).then(function (result) {

          res = confirm("La factura se guardó con éxito, ¿Desea Imprimirla?");

          if (res) {
            resetBill();
            $state.go("dashboard.generateBill", {
              "code": result.data.code
            });
          } else {
            resetBill();
          }

        }, function (err) {
          console.log("Error al generar factura");
          console.log(err);
        });
      }
    }

    $scope.deleteItem = function (item_index) {
      for (var i = 0; i < $scope.billProducts.length; i++) {
        if ($scope.billProducts[i].id_index === item_index) {
          $scope.billProducts.splice(i, 1);
          if ($scope.billProducts.length == 0) {
            index = 0;
          }
          getValues();
          //console.table($scope.billProducts);
          break;
        }
      }
    }

    //***************** GENERAL USE FUNTIONS *********************

    /**
     * @description: function to scroll until some validation fields
     **/
    function scrolltop() {
      $('html, body').animate({
        scrollTop: 0
      }, 800);
    }

    /**
     * @description: function to check if an object is empty
     **/
    function isEmpty(obj) {
      for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
          return false;
      }
      return true;
    }

    /**
     * @description: function to reverse array printing
     **/
    $scope.reverse = function (array) {
      var copy = [].concat(array);
      return copy.reverse();
    }

    construct();
  });