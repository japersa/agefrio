angular.module('serviceView', [])

.controller('serviceCtrl', function ($scope, $api, $timeout) {

    //Init variables
    $scope.alertTemplate = "";
    $scope.showSuccessNotification = false;
    $scope.showDangerNotification = false;
    //Load all services
    loadMainData();

    //*************************************************************************
    /**
     * @description: load all services
     *
     **/

    function loadMainData() {

        $api.getServices().then(function (result) {
            $scope.services = result.data;
        }, function (err) {
            alert("Ha ocurrido un error al cargar los servicios");
        });

    }

    //*************************************************************************
    /**
     * @description: open modal for add new service, addnew function
     *
     **/

    $scope.addNewModal = function () {
        $scope.codeService = $api.codeGen();
        $api.getServiceCode().then(function (result) {
            $scope.ref_code = result.data.ref;
        }, function (err) {
            console.log("Ha ocurrido un error al generar la referencia del servicio");
        });
    };

    $scope.addNew = function (service) {

        if ($('#reference').val() == '') {
            alert('Ocurrio un error al crear la referencia del servicio');
        } else if ($('#name').val() == '') {
            alert('Ingrese el nombre del servicio');
        } else if ($('#service_value').val() == '') {
            alert('Ingrese el valor del servicio');
        } else {

            var res = confirm("Deseas guardar este servicio?");

            if (res) {
                $api.addNewService({
                    ref: $scope.ref_code,
                    code: $scope.codeService,
                    name: service.name,
                    description: service.description,
                    price: service.price
                }).then(function (result) {
                    $('#serviceModal').modal('hide');
                    $scope.service = {};
                    $scope.ref_code = "";
                    $scope.showSuccessNotification = true;
                    $scope.alertTemplate = "El servicio se ha guardado con exito";
                    $timeout(dismissAlert, 3000);
                    loadMainData();
                }, function (err) {
                    $('#serviceModal').modal('hide');
                    $scope.service = {};
                    $scope.showDangerNotification = true;
                    $scope.alertTemplate = "Ocurrio un error al guardar el servicio";
                    $timeout(dismissAlert, 3000);
                });
            }
        }

    }

    //*************************************************************************
    /**
     * @description: open modal for edit service, update service function
     *
     **/
    $scope.editModal = function (service) {
        $scope.editService = angular.copy(service);
        $('#editServiceModal').modal('show');
    };

    $scope.edit = function (code) {

        if ($('#name_edit').val() == '') {
            alert('Ingrese el nombre del servicio');
        } else if ($('#service_value_edit').val() == '') {
            alert('Ingrese el valor del servicio');
        } else {

            var res = confirm("¿Deseas actualizar este servicio?");

            if (res) {
                $api.updateService(code, {
                    name: $scope.editService.name,
                    description: $scope.editService.description,
                    price: $scope.editService.price
                }).then(function (result) {
                    $('#editServiceModal').modal('hide');
                    $scope.resetFormEdit();
                    templateAction("El servicio se actualizo con exito");
                }, function (err) {
                    $('#editServiceModal').modal('hide');
                    $scope.resetFormEdit();
                    $scope.showDangerNotification = true;
                    $scope.alertTemplate = "Ocurrio un error al actualizar el servicio";
                    $timeout(dismissAlert, 3000);
                })

            }
        }


    };

    //*************************************************************************
    /**
     * @description: activate/desactivate service function
     *
     **/

    $scope.desactivate = function (code) {

        var res = confirm("Deseas desactivar este servicio?");

        if (res) {
            $api.changeServiceStatus(code, {
                status: 2
            }).then(function (result) {
                templateAction("Se ha desactivado el servicio");
            }, function (err) {
                templateDanger("Ocurrio un error al desactivar el servicio");
            })
        }
    };

    $scope.activateService = function (code) {
        var res = confirm("Deseas activar este servicio?");

        if (res) {
            $api.changeServiceStatus(code, {
                status: 1
            }).then(function (result) {
                templateAction("El servicio se activado con exito");
            }, function (err) {
                templateDanger("Ocurrio un error al activar el servicio");
            })
        }
    };

    //*************************************************************************
    /**
     * @description: util functions
     *
     **/

    $scope.resetFormEdit = function () {
        $scope.editService.name = "";
        $scope.editService.description = "";
        $scope.editService.price = "";
    };

    $scope.resetFormCreateNew = function () {
        $scope.service.name = "";
        $scope.service.description = "";
        $scope.service.price = "";
    };

    function templateAction(message) {
        $scope.showSuccessNotification = true;
        $scope.alertTemplate = message;
        $timeout(dismissAlert, 3000);
        loadMainData();
    }

    function templateDanger(message) {
        $scope.showDangerNotification = true;
        $scope.alertTemplate = message;
        $timeout(dismissAlert, 3000);
    }

    function dismissAlert() {
        $scope.showSuccessNotification = false;
        $scope.showDangerNotification = false;
        $scope.alertTemplate = "";
    }

    /**
     * @description: function to reverse array printing
     **/
    $scope.reverse = function (array) {
        var copy = [].concat(array);
        return copy.reverse();
    }


});