'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
    .controller('MainCtrl', function ($scope, $api) {
      /**
       * @description: function to load bill config
       **/
      $api.getIndex().then(function (result) {
          $scope.index = result.data;
      },function (err) {
          console.log("Error al cargar index");
      });

    });
