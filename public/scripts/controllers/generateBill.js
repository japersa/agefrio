angular.module('generateBill', [])

.controller('generateBillCtrl', function ($scope, $stateParams, $api, $num2text) {

    /**
     * @description: function to call print view with the content of the html bill
     **/
    $scope.printDiv = function () {


        document.body.style.backgroundColor = "white";

        var divName = "printableArea";
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;

        setTimeout(function () {
            $(window).one('mousemove', window.onafterprint);
        }, 1);

    }

    /**
     * @description: call function after print close
     **/
    window.onafterprint = function (e) {
        $(window).off('mousemove', window.onafterprint);
        document.body.style.backgroundColor = "#f8f8f8";
        location.reload();
    };


    /**
     * @description: filter to parse day in format yyyy/mm/dd
     **/
    $scope.getDateFormat = function (timestamp) {
        var d = new Date(timestamp);
        var day = ("0" + d.getDate()).slice(-2);
        var month = ("0" + (d.getMonth() + 1)).slice(-2);
        var year = d.getFullYear();
        return day + "/" + month + "/" + year;
    }


    /**
     * @description: function to load bill settings
     **/
    getBillSettings();

    function getBillSettings() {
        $api.getSetting('bill').then(function (result) {
            $scope.settings = result.data;
        }, function (err) {
            console.log("Error al cargar los ajustes");
        });
    }

    $api.getBill($stateParams.code).then(function (result) {
        $scope.bill = result.data;
        blankSpaceGenerator($scope.bill.products.length);
    }, function (err) {
        console.log("Error al cargar la factura");
    });

    /**
     * @description: function to convert total bill amount in letters
     **/
    $scope.convert = function (number) {
        return $num2text.convert(parseInt(number));
    }

    function blankSpaceGenerator(number) {

        // Max amount of items in first page: 10 con footer 18 sin footer
        // Max amount of items in second page and beyond: 25 CON FOOTER 34 SIN FOOTER     
        // number: equals to amount of products to print

        switch (number) {
        case 1:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9];
            break;
        case 2:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8];
            break;
        case 3:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7];
            break;
        case 4:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6];
            break;
        case 5:
            $scope.blankspaces = [1, 2, 3, 4, 5];
            break;
        case 6:
            $scope.blankspaces = [1, 2, 3, 4];
            break;
        case 7:
            $scope.blankspaces = [1, 2, 3];
            break;
        case 8:
            $scope.blankspaces = [1, 2];
            break;
        case 9:
            $scope.blankspaces = [1];
            break;
        case 10:
            $scope.blankspaces = [];
            break;
        case 11:
            // 7:page 1 - 25:page 2
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32];
            break;
        case 12:
            // 6:page 1 - 25:page 2
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
            break;
        case 13:
            // 5:page 1 - 25:page 2
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
            break;
        case 14:
            // 4:page 1 - 25:page 2
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
            break;
        case 15:
            // 3:page 1 - 25:page 2
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28];
            break;
        case 16:
            // 2:page 1 - 25:page 2
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27];
            break;
        case 17:
            // 1:page 1 - 25:page 2
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26];
            break;
        case 18:
            // 0:page 1 - 25:page 2
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];
            break;
        case 19:
            /* ___________PAGINA 2___________ */
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
            break;
        case 20:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
            break;
        case 21:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];
            break;
        case 22:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21];
            break;
        case 23:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
            break;
        case 24:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
            break;
        case 25:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
            break;
        case 26:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
            break;
        case 27:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
            break;
        case 28:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
            break;
        case 29:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
            break;
        case 30:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
            break;
        case 31:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
            break;
        case 32:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
            break;
        case 33:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            break;
        case 34:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9];
            break;
        case 35:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8];
            break;
        case 36:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7];
            break;
        case 37:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6];
            break;
        case 38:
            $scope.blankspaces = [1, 2, 3, 4, 5];
            break;
        case 39:
            $scope.blankspaces = [1, 2, 3, 4];
            break;
        case 40:
            $scope.blankspaces = [1, 2, 3];
            break;
        case 41:
            $scope.blankspaces = [1, 2];
            break;
        case 42:
            $scope.blankspaces = [1];
            break;
        case 43:
            $scope.blankspaces = [];
            break;
        case 44:
            // 9 disponibles sin el footer page 2 / 25 page 3
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34];
            break;
        case 45:
            // 8 disponibles sin el footer page 2 / 25 page 3
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33];
            break;
        case 46:
            // 7 disponibles sin el footer page 2 / 25 page 3
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32];
            break;
        case 47:
            // 6 disponibles sin el footer page 2 / 25 page 3
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
            break;
        case 48:
            // 5 disponibles sin el footer page 2 / 25 page 3
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
            break;
        case 49:
            // 4 disponibles sin el footer page 2 / 25 page 3
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
            break;
        case 50:
            // 3 disponibles sin el footer page 2 / 25 page 3
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28];
            break;
        case 51:
            // 2 disponibles sin el footer page 2 / 25 page 3
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27];
            break;
        case 52:
            // 1 disponibles sin el footer page 2 / 25 page 3
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26];
            break;
        case 53:
            /* PAGE 3*/
            // 0 disponibles sin el footer page 2 / 24 page 3
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];
            break;
        case 54:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
            break;
        case 55:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
            break;
        case 56:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];
            break;
        case 57:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21];
            break;
        case 58:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
            break;
        case 59:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
            break;
        case 60:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
            break;
        case 61:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
            break;
        case 62:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
            break;
        case 63:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
            break;
        case 64:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
            break;
        case 65:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
            break;
        case 66:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
            break;
        case 67:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
            break;
        case 68:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            break;
        case 69:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8, 9];
            break;
        case 70:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7, 8];
            break;
        case 71:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6, 7];
            break;
        case 72:
            $scope.blankspaces = [1, 2, 3, 4, 5, 6];
            break;
        case 73:
            $scope.blankspaces = [1, 2, 3, 4, 5];
            break;
        case 74:
            $scope.blankspaces = [1, 2, 3, 4];
            break;
        case 75:
            $scope.blankspaces = [1, 2, 3];
            break;
        case 76:
            $scope.blankspaces = [1, 2];
            break;
        case 77:
            $scope.blankspaces = [1];
            break;
        case 78:
            $scope.blankspaces = [];
            break;

        }
    }


});