angular.module('customers', [])

.controller('customerCtrl', function ($scope, $api, $timeout) {


    //Init variables
    $scope.alertTemplate = "";
    $scope.showSuccessNotification = false;
    $scope.showDangerNotification = false;
    //Load all products
    loadMainData();

    //*************************************************************************
    /**
     * @description: load all customers
     * 
     **/

    function loadMainData() {

        $api.getCustomers().then(function (result) {
            $scope.customers = result.data;
            if (!$scope.identification_types) {
                $api.getIdentificationTypes().then(function (result) {
                    $scope.identification_types = result.data;
                }, function () {
                    console.log("Error al cargar tipos de documento");
                })
            }

        }, function (err) {
            alert("Ha ocurrido un error al cargar los clientes");
        });

    }

    //*************************************************************************
    /**
     * @description: open modal for add new customer, addnew function
     * 
     **/

    $scope.addNewModal = function () {
        $scope.codeCustomer = $api.codeGen();
    };

    $scope.addNew = function (customer) {

        var identification_type = $scope.identification_type;

        if (identification_type == '') {
            alert('Seleccione el tipo de identificación');
        } else if ($('#id_document').val() == '') {
            alert('Ingrese el número de identificación');
        } else if ($('#name').val() == '') {
            alert('Ingrese el nombre del cliente');
        } else if ($('#email').val() == '') {
            alert('Ingrese el correo del cliente');
        } else if ($('#phone').val() == '') {
            alert('Ingrese el número teléfonico del cliente');
        } else {

            var res = confirm("Deseas guardar este cliente?");

            if (res) {
                $api.addNewCustomer({
                    code: $scope.codeCustomer,
                    name: customer.name,
                    identification_type: identification_type,
                    id_document: customer.id_document,
                    email: customer.email,
                    address: customer.address,
                    phone: customer.phone,
                    mobile_phone: customer.mobile_phone,
                    city: customer.city
                }).then(function (result) {
                    $('#customersModal').modal('hide');
                    $scope.customer = {};
                    $scope.showSuccessNotification = true;
                    $scope.alertTemplate = "El cliente se ha guardado con exito";
                    $timeout(dismissAlert, 3000);
                    loadMainData();
                }, function (err) {
                    alert(JSON.stringify(err));
                    $('#customersModal').modal('hide');
                    $scope.customer = {};
                    $scope.showDangerNotification = true;
                    $scope.alertTemplate = "Ocurrio un error al guardar el cliente";
                    $timeout(dismissAlert, 3000);
                });
            }
        }

    }

    //*************************************************************************
    /**
     * @description: open modal for edit customer, update customer function
     * 
     **/
    $scope.editModal = function (customer) {
        $scope.editCustomer = angular.copy(customer);
        $('#editCustomersModal').modal('show');
    };

    $scope.edit = function (code) {

        var identification_type = $scope.editCustomer.identification_type;

        if (identification_type == '') {
            alert('Seleccione el tipo de identificación');
        } else if ($('#id_documentEdit').val() == '') {
            alert('Ingrese el número de identificación');
        } else if ($('#nameEdit').val() == '') {
            alert('Ingrese el nombre del cliente');
        } else if ($('#emailEdit').val() == '') {
            alert('Ingrese el correo del cliente');
        } else if ($('#phoneEdit').val() == '') {
            alert('Ingrese el número teléfonico del cliente');
        } else {

            var res = confirm("Deseas actualizar el cliente: " + $scope.editCustomer.name + " ?");

            if (res) {
                $api.updateCustomer($scope.editCustomer.code, {
                    name: $scope.editCustomer.name,
                    email: $scope.editCustomer.email,
                    identification_type: identification_type,
                    id_document: $scope.editCustomer.id_document,
                    address: $scope.editCustomer.address,
                    phone: $scope.editCustomer.phone,
                    mobile_phone: $scope.editCustomer.mobile_phone,
                    city: $scope.editCustomer.city
                }).then(function (result) {
                    $('#editCustomersModal').modal('hide');
                    $scope.resetFormEdit();
                    templateAction("El cliente se actualizo con exito");
                }, function (err) {
                    alert(JSON.stringify(err));
                    $('#editCustomersModal').modal('hide');
                    $scope.resetFormEdit();
                    $scope.showDangerNotification = true;
                    $scope.alertTemplate = "Ocurrio un error al actualizar el cliente";
                    $timeout(dismissAlert, 3000);
                })

            }


        }


    };

    //*************************************************************************
    /**
     * @description: activate/desactivate product function
     * 
     **/

    $scope.desactivate = function (code) {

        var res = confirm("Deseas desactivar el cliente con el código: " + code + " ?");

        if (res) {
            $api.changeCustomerStatus(code, {
                status: 2
            }).then(function (result) {
                templateAction("Se ha desactivado el cliente");
            }, function (err) {
                templateDanger("Ocurrio un error al desactivar el cliente");
            })
        }
    };

    $scope.activate = function (code) {
        var res = confirm("Deseas activar el cliente con el código: " + code + " ?");

        if (res) {
            $api.changeCustomerStatus(code, {
                status: 1
            }).then(function (result) {
                templateAction("El cliente se activo con exito");
            }, function (err) {
                templateDanger("Ocurrio un error al activar el cliente");
            })
        }
    };

    //*************************************************************************
    /**
     * @description: util functions
     * 
     **/

    $scope.resetFormEdit = function () {
        $scope.editCustomer.id_document = "";
        $scope.editCustomer.identification_type = "";
        $scope.editCustomer.name = "";
        $scope.editCustomer.address = "";
        $scope.editCustomer.phone = "";
        $scope.editCustomer.mobile_phone = "";
        $scope.editCustomer.email = "";
        $scope.editCustomer.city = "";
    };

    $scope.resetFormCreateNew = function () {

        $scope.customer.id_document = "";
        $scope.customer.identification_type = "";
        $scope.customer.name = "";
        $scope.customer.address = "";
        $scope.customer.phone = "";
        $scope.customer.mobile_phone = "";
        $scope.customer.email = "";
        $scope.customer.city = "";
    };

    function templateAction(message) {
        $scope.showSuccessNotification = true;
        $scope.alertTemplate = message;
        $timeout(dismissAlert, 3000);
        loadMainData();
    }

    function templateDanger(message) {
        $scope.showDangerNotification = true;
        $scope.alertTemplate = message;
        $timeout(dismissAlert, 3000);
    }

    function dismissAlert() {
        $scope.showSuccessNotification = false;
        $scope.showDangerNotification = false;
        $scope.alertTemplate = "";
    }

    /**
     * @description: function to reverse array printing
     **/
    $scope.reverse = function (array) {
        var copy = [].concat(array);
        return copy.reverse();
    }

});