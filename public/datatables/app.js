angular.module('starter', ['datatables', 'datatables.bootstrap'])

.controller('starterCtrl', function($scope, DTOptionsBuilder, DTColumnBuilder){
  // dtOptions
  $scope.dtOptions = DTOptionsBuilder
    .fromSource('path')
    .withBootstrap()
    .withLanguage({
      sUrl: 'https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json'
    })

  // dtColumns
  $scope.dtColumns = [
    DTColumnBuilder.newColumn('_id').withTitle('ID'),
    DTColumnBuilder.newColumn('name').withTitle('Name'),
    DTColumnBuilder.newColumn('email').withTitle('Email')
  ];

})
